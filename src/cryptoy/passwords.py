import hashlib
import os
from random import (
    Random,
)

import names


def hash_password(password: str) -> str:
	return hashlib.sha3_256(password.encode()).hexdigest()


def random_salt() -> str:
	return bytes.hex(os.urandom(32))


def generate_users_and_password_hashes(
    passwords: list[str], count: int = 32
) -> dict[str, str]:
	rng = Random()  # noqa: S311

	users_and_password_hashes = {
	names.get_full_name(): hash_password(rng.choice(passwords))
	for _i in range(count)
	}
	return users_and_password_hashes


def attack(passwords: list[str], passwords_database: dict[str, str]) -> dict[str, str]:
	users_and_passwords = {}

	# A implémenter
	# Doit calculer le mots de passe de chaque utilisateur grace à une attaque par dictionnaire

	rockyou = {}
	for p in passwords:
		rockyou[p] = hash_password(p)

	for username, real_pass in passwords_database.items():
		for p, h in rockyou.items():
			if h == real_pass:
				users_and_passwords[username] = p
				break
	return users_and_passwords


def fix(
    passwords: list[str], passwords_database: dict[str, str]
) -> dict[str, dict[str, str]]:
	users_and_passwords = attack(passwords, passwords_database)

	users_and_salt = {}
	new_database = {}

	for user, pwd in users_and_passwords.items():
		s = random_salt()
		s_pwd = hash_password(s + pwd)
		new_database[user] = {'password_hash': s_pwd, 'password_salt': s}

	# A implémenter
	# Doit calculer une nouvelle base de donnée ou chaque élement est un dictionnaire de la forme:
	# {
	#     "password_hash": H,
	#     "password_salt": S,
	# }
	# tel que H = hash_password(S + password)

	return new_database


def authenticate(
    user: str, password: str, new_database: dict[str, dict[str, str]]
) -> bool:
	# Doit renvoyer True si l'utilisateur a envoyé le bon password, False sinon
	if user not in new_database:
		return False

	user_dict = new_database[user]
	haché = user_dict["password_hash"]
	salé = user_dict["password_salt"]
	
	# Penser à bien saler avant de hacher
	pwd_h = hash_password(salé + password)
	if pwd_h == haché:
		return True
	else:
		return False
